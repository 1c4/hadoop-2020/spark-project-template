# Compounds a string of values separated by a given separator.
# Usage example:
#   classpath="$(mk_string : lib1.jar lib2.jar lib3.jar)"
#  result:
#   lib1.jar:lib2.jar:lib3.jar
function mk_string() {
  local IFS="$1"
  shift
  echo "$*"
}
