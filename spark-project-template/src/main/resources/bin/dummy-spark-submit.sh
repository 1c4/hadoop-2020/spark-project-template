#!/usr/bin/env bash

set -o nounset
set -o errexit
set -o pipefail

bin_dir=
if echo "$0" | grep -q /
then
  # Started with absolute or relative path
  bin_dir="$(cd "$(dirname -- "$0")" ; pwd)"
else
  # Started from PATH
  bin_dir="$(cd "$(dirname -- "$(command -v "$0")")" ; pwd)"
fi
base_dir="$(dirname -- "$bin_dir")"
libexec_dir="$base_dir/libexec"
lib_dir="$base_dir/lib"
conf_dir="$base_dir/conf"

source "$libexec_dir/lib-log.sh"
source "$libexec_dir/lib-util.sh"
source "$conf_dir/env.sh"

spark_conf_file="$conf_dir/spark.conf"
log_conf_file_name="log4j.xml"
log_conf_file="$conf_dir/$log_conf_file_name"

app_jar="$lib_dir/spark-project-template-apps-@{project.version}.jar"
jars="$(mk_string , $(find "$lib_dir" -type f -name \*.jar | grep -v "spark-project-template-apps"))"
app_main_class="org.fivt.atp.bigdata.dummy.DummyMain"
app_name="${app_main_class##*.}"

"$SPARK_HOME/bin/spark-submit" \
  -v \
  --properties-file "$spark_conf_file" \
  --jars "$jars" \
  --class "$app_main_class" \
  --name "$app_name" \
  --master yarn \
  --deploy-mode cluster \
  --files "$log_conf_file" \
  --driver-java-options "-Dlog4j.configuration=$log_conf_file_name" \
  --conf spark.executor.extraJavaOptions="-Dlog4j.configuration=$log_conf_file_name" \
  "$app_jar"
